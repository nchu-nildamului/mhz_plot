#!-*- coding:utf8 -*-
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from mpl_toolkits.mplot3d.axes3d import Axes3D, get_test_data


# CHART_MHZ = '593 MHz' # 改以問答式取得參數
Z_LOWER_BOUND = 35
Z_UPPER_BOUND = 60
COLOR_BAR_LABEL = u'dB μV'
X_LABEL = u'm'
Y_LABEL = u'm'
Z_LABEL = u'dB μV'


# Read Data
file = open('./data.csv')
content =  file.readlines()

# Process Title
title = content[0].split(',')
title = title[:-1] if title[-1] == '' else title #s1
'''
#s1 is short for:

if title[-1] == '':
    title = title[:-1]
else:
    title = title
'''

MHZ_mapping = {}
MHZ_options = []
quest = u'Please select a MHz for plot:\n'

for index, mhz in enumerate(title[2:]):
    MHZ_mapping[mhz.strip()] = index
    MHZ_options.append(mhz.strip())
    quest += '(%s) %s\n' % (index, mhz)

mhz_index = -1
while mhz_index not in xrange(0, len(MHZ_options)):
    try: mhz_index = int(raw_input(quest))
    except: pass
CHART_MHZ = MHZ_options[mhz_index]


# Process Data
data = [i.strip().split(',') for i in content[1:]] #s2
data = [[float(n) for n in set] for set in data] #s3
data.sort()
'''
#s2 is short for:

tmp = []
for i in content[1:]:
    data.append(i.strip().split(','))
data = tmp


#s3 is short for:
tmp = []
for set in data:
    number = []
    for n in set:
        number.append(int(n))
    tmp.append(number)
data = tmp
'''

x_holder, y_holder, z_holder = [], [], []
x_ax, y_ax = [], []
for row in data:
    x, y = row[:2]
    z = row[2:]
    z = z[:-1] if z[-1] == '' else z

    if x not in x_ax: x_ax.append(x)
    if y not in y_ax: y_ax.append(y)

    if x not in x_holder:
        if y_holder:
            z_holder.append(y_holder)

        x_holder.append(x)
        y_holder = []

    y_holder.append(int(z[MHZ_mapping[CHART_MHZ]]))
z_holder.append(y_holder)

z_array = np.array(z_holder)
z_array = np.transpose(z_array)


# Plot
matplotlib.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
fig = plt.figure(figsize=plt.figaspect(0.5))
ax = fig.add_subplot(1, 1, 1, projection='3d')

X, Y = np.array(x_ax), np.array(y_ax)
X, Y = np.meshgrid(X, Y)
Z = z_array


plt.title(CHART_MHZ, position=(0.5, 0))
plt.xticks(x_ax) # X 軸標空間範圍
plt.yticks(y_ax[::2]) # Y 軸標空間範圍

ax.set_xticklabels(x_ax, rotation=45) # X 軸標示的值與角度
ax.set_yticklabels(y_ax[::2], rotation=45) # Y 軸標示的值與角度
ax.xaxis.labelpad = 20 # X 軸標示與值的距離
ax.yaxis.labelpad = 20 # Y 軸標示的值的距離

ax.set_xlabel(X_LABEL, fontsize=16)
ax.set_ylabel(Y_LABEL, fontsize=16)
ax.set_zlabel(Z_LABEL, fontsize=16, rotation=45)
ax.set_zlim(Z_LOWER_BOUND, Z_UPPER_BOUND)

surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.summer, linewidth=0, antialiased=False)
'''
cmap (color map) 參數為色彩參數，以下為 matplotlib 預設的色彩樣板(e.g. cmap=cm.Accent)：
        Accent = <matplotlib.colors.LinearSegmentedColormap object>
        Accent_r = <matplotlib.colors.LinearSegmentedColormap object>
        Blues = <matplotlib.colors.LinearSegmentedColormap object>
        Blues_r = <matplotlib.colors.LinearSegmentedColormap object>
        BrBG = <matplotlib.colors.LinearSegmentedColormap object>
        BrBG_r = <matplotlib.colors.LinearSegmentedColormap object>
        BuGn = <matplotlib.colors.LinearSegmentedColormap object>
        BuGn_r = <matplotlib.colors.LinearSegmentedColormap object>
        BuPu = <matplotlib.colors.LinearSegmentedColormap object>
        BuPu_r = <matplotlib.colors.LinearSegmentedColormap object>
        CMRmap = <matplotlib.colors.LinearSegmentedColormap object>
        CMRmap_r = <matplotlib.colors.LinearSegmentedColormap object>
        Dark2 = <matplotlib.colors.LinearSegmentedColormap object>
        Dark2_r = <matplotlib.colors.LinearSegmentedColormap object>
        GnBu = <matplotlib.colors.LinearSegmentedColormap object>
        GnBu_r = <matplotlib.colors.LinearSegmentedColormap object>
        Greens = <matplotlib.colors.LinearSegmentedColormap object>
        Greens_r = <matplotlib.colors.LinearSegmentedColormap object>
        Greys = <matplotlib.colors.LinearSegmentedColormap object>
        Greys_r = <matplotlib.colors.LinearSegmentedColormap object>
        OrRd = <matplotlib.colors.LinearSegmentedColormap object>
        OrRd_r = <matplotlib.colors.LinearSegmentedColormap object>
        Oranges = <matplotlib.colors.LinearSegmentedColormap object>
        Oranges_r = <matplotlib.colors.LinearSegmentedColormap object>
        PRGn = <matplotlib.colors.LinearSegmentedColormap object>
        PRGn_r = <matplotlib.colors.LinearSegmentedColormap object>
        Paired = <matplotlib.colors.LinearSegmentedColormap object>
        Paired_r = <matplotlib.colors.LinearSegmentedColormap object>
        Pastel1 = <matplotlib.colors.LinearSegmentedColormap object>
        Pastel1_r = <matplotlib.colors.LinearSegmentedColormap object>
        Pastel2 = <matplotlib.colors.LinearSegmentedColormap object>
        Pastel2_r = <matplotlib.colors.LinearSegmentedColormap object>
        PiYG = <matplotlib.colors.LinearSegmentedColormap object>
        PiYG_r = <matplotlib.colors.LinearSegmentedColormap object>
        PuBu = <matplotlib.colors.LinearSegmentedColormap object>
        PuBuGn = <matplotlib.colors.LinearSegmentedColormap object>
        PuBuGn_r = <matplotlib.colors.LinearSegmentedColormap object>
        PuBu_r = <matplotlib.colors.LinearSegmentedColormap object>
        PuOr = <matplotlib.colors.LinearSegmentedColormap object>
        PuOr_r = <matplotlib.colors.LinearSegmentedColormap object>
        PuRd = <matplotlib.colors.LinearSegmentedColormap object>
        PuRd_r = <matplotlib.colors.LinearSegmentedColormap object>
        Purples = <matplotlib.colors.LinearSegmentedColormap object>
        Purples_r = <matplotlib.colors.LinearSegmentedColormap object>
        RdBu = <matplotlib.colors.LinearSegmentedColormap object>
        RdBu_r = <matplotlib.colors.LinearSegmentedColormap object>
        RdGy = <matplotlib.colors.LinearSegmentedColormap object>
        RdGy_r = <matplotlib.colors.LinearSegmentedColormap object>
        RdPu = <matplotlib.colors.LinearSegmentedColormap object>
        RdPu_r = <matplotlib.colors.LinearSegmentedColormap object>
        RdYlBu = <matplotlib.colors.LinearSegmentedColormap object>
        RdYlBu_r = <matplotlib.colors.LinearSegmentedColormap object>
        RdYlGn = <matplotlib.colors.LinearSegmentedColormap object>
        RdYlGn_r = <matplotlib.colors.LinearSegmentedColormap object>
        Reds = <matplotlib.colors.LinearSegmentedColormap object>
        Reds_r = <matplotlib.colors.LinearSegmentedColormap object>
        Set1 = <matplotlib.colors.LinearSegmentedColormap object>
        Set1_r = <matplotlib.colors.LinearSegmentedColormap object>
        Set2 = <matplotlib.colors.LinearSegmentedColormap object>
        Set2_r = <matplotlib.colors.LinearSegmentedColormap object>
        Set3 = <matplotlib.colors.LinearSegmentedColormap object>
        Set3_r = <matplotlib.colors.LinearSegmentedColormap object>
        Spectral = <matplotlib.colors.LinearSegmentedColormap object>
        Spectral_r = <matplotlib.colors.LinearSegmentedColormap object>
        Wistia = <matplotlib.colors.LinearSegmentedColormap object>
        Wistia_r = <matplotlib.colors.LinearSegmentedColormap object>
        YlGn = <matplotlib.colors.LinearSegmentedColormap object>
        YlGnBu = <matplotlib.colors.LinearSegmentedColormap object>
        YlGnBu_r = <matplotlib.colors.LinearSegmentedColormap object>
        YlGn_r = <matplotlib.colors.LinearSegmentedColormap object>
        YlOrBr = <matplotlib.colors.LinearSegmentedColormap object>
        YlOrBr_r = <matplotlib.colors.LinearSegmentedColormap object>
        YlOrRd = <matplotlib.colors.LinearSegmentedColormap object>
        YlOrRd_r = <matplotlib.colors.LinearSegmentedColormap object>
        afmhot = <matplotlib.colors.LinearSegmentedColormap object>
        afmhot_r = <matplotlib.colors.LinearSegmentedColormap object>
        autumn = <matplotlib.colors.LinearSegmentedColormap object>
        autumn_r = <matplotlib.colors.LinearSegmentedColormap object>
        binary = <matplotlib.colors.LinearSegmentedColormap object>
        binary_r = <matplotlib.colors.LinearSegmentedColormap object>
        bone = <matplotlib.colors.LinearSegmentedColormap object>
        bone_r = <matplotlib.colors.LinearSegmentedColormap object>
        brg = <matplotlib.colors.LinearSegmentedColormap object>
        brg_r = <matplotlib.colors.LinearSegmentedColormap object>
        bwr = <matplotlib.colors.LinearSegmentedColormap object>
        bwr_r = <matplotlib.colors.LinearSegmentedColormap object>
        cool = <matplotlib.colors.LinearSegmentedColormap object>
        cool_r = <matplotlib.colors.LinearSegmentedColormap object>
        coolwarm = <matplotlib.colors.LinearSegmentedColormap object>
        coolwarm_r = <matplotlib.colors.LinearSegmentedColormap object>
        copper = <matplotlib.colors.LinearSegmentedColormap object>
        copper_r = <matplotlib.colors.LinearSegmentedColormap object>
        cubehelix = <matplotlib.colors.LinearSegmentedColormap object>
        cubehelix_r = <matplotlib.colors.LinearSegmentedColormap object>
        flag = <matplotlib.colors.LinearSegmentedColormap object>
        flag_r = <matplotlib.colors.LinearSegmentedColormap object>
        gist_earth = <matplotlib.colors.LinearSegmentedColormap object>
        gist_earth_r = <matplotlib.colors.LinearSegmentedColormap object>
        gist_gray = <matplotlib.colors.LinearSegmentedColormap object>
        gist_gray_r = <matplotlib.colors.LinearSegmentedColormap object>
        gist_heat = <matplotlib.colors.LinearSegmentedColormap object>
        gist_heat_r = <matplotlib.colors.LinearSegmentedColormap object>
        gist_ncar = <matplotlib.colors.LinearSegmentedColormap object>
        gist_ncar_r = <matplotlib.colors.LinearSegmentedColormap object>
        gist_rainbow = <matplotlib.colors.LinearSegmentedColormap object>
        gist_rainbow_r = <matplotlib.colors.LinearSegmentedColormap object>
        gist_stern = <matplotlib.colors.LinearSegmentedColormap object>
        gist_stern_r = <matplotlib.colors.LinearSegmentedColormap object>
        gist_yarg = <matplotlib.colors.LinearSegmentedColormap object>
        gist_yarg_r = <matplotlib.colors.LinearSegmentedColormap object>
        gnuplot = <matplotlib.colors.LinearSegmentedColormap object>
        gnuplot2 = <matplotlib.colors.LinearSegmentedColormap object>
        gnuplot2_r = <matplotlib.colors.LinearSegmentedColormap object>
        gnuplot_r = <matplotlib.colors.LinearSegmentedColormap object>
        gray = <matplotlib.colors.LinearSegmentedColormap object>
        gray_r = <matplotlib.colors.LinearSegmentedColormap object>
        hot = <matplotlib.colors.LinearSegmentedColormap object>
        hot_r = <matplotlib.colors.LinearSegmentedColormap object>
        hsv = <matplotlib.colors.LinearSegmentedColormap object>
        hsv_r = <matplotlib.colors.LinearSegmentedColormap object>
        inferno = <matplotlib.colors.ListedColormap object>
        inferno_r = <matplotlib.colors.ListedColormap object>
        jet = <matplotlib.colors.LinearSegmentedColormap object>
        jet_r = <matplotlib.colors.LinearSegmentedColormap object>
        magma = <matplotlib.colors.ListedColormap object>
        magma_r = <matplotlib.colors.ListedColormap object>
        nipy_spectral = <matplotlib.colors.LinearSegmentedColormap object>
        nipy_spectral_r = <matplotlib.colors.LinearSegmentedColormap object>
        ocean = <matplotlib.colors.LinearSegmentedColormap object>
        ocean_r = <matplotlib.colors.LinearSegmentedColormap object>
        pink = <matplotlib.colors.LinearSegmentedColormap object>
        pink_r = <matplotlib.colors.LinearSegmentedColormap object>
        plasma = <matplotlib.colors.ListedColormap object>
        plasma_r = <matplotlib.colors.ListedColormap object>
        prism = <matplotlib.colors.LinearSegmentedColormap object>
        prism_r = <matplotlib.colors.LinearSegmentedColormap object>
        rainbow = <matplotlib.colors.LinearSegmentedColormap object>
        rainbow_r = <matplotlib.colors.LinearSegmentedColormap object>
        seismic = <matplotlib.colors.LinearSegmentedColormap object>
        seismic_r = <matplotlib.colors.LinearSegmentedColormap object>
        spectral = <matplotlib.colors.LinearSegmentedColormap object>
        spectral_r = <matplotlib.colors.LinearSegmentedColormap object>
        spring = <matplotlib.colors.LinearSegmentedColormap object>
        spring_r = <matplotlib.colors.LinearSegmentedColormap object>
        summer = <matplotlib.colors.LinearSegmentedColormap object>
        summer_r = <matplotlib.colors.LinearSegmentedColormap object>
        terrain = <matplotlib.colors.LinearSegmentedColormap object>
        terrain_r = <matplotlib.colors.LinearSegmentedColormap object>
        viridis = <matplotlib.colors.ListedColormap object>
        viridis_r = <matplotlib.colors.ListedColormap object>
        winter = <matplotlib.colors.LinearSegmentedColormap object>
        winter_r = <matplotlib.colors.LinearSegmentedColormap object>
'''

cb = fig.colorbar(surf, shrink=0.5, aspect=10)
cb.set_label(COLOR_BAR_LABEL)

plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)
plt.show()